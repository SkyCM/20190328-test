package database;

import database.generated.GeneratedMydbApplicationImpl;

/**
 * The default {@link com.speedment.runtime.core.Speedment} implementation class
 * for the {@link com.speedment.runtime.config.Project} named mydb.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class MydbApplicationImpl 
extends GeneratedMydbApplicationImpl 
implements MydbApplication {}